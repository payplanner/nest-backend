FROM node:14-alpine

RUN apk update && apk add --no-cache \
    nss \
    freetype \
    freetype-dev \
    harfbuzz \
    ca-certificates \
    ttf-freefont \
    nodejs-current \
    yarn

# Create app directory
COPY . /app
WORKDIR /app

# Install app dependencies

RUN npm install --production=false
# Run the app
CMD ["npm","run", "start:dev"]